#!/usr/bin/env python
import sqlite3
import datetime
import os 

try:
    os.remove("hijackers.txt") 
except:
    pass

try:
    os.remove("victims.txt") 
except:
    pass

conn = sqlite3.connect('record.db')
c = conn.cursor()

res1 = c.execute('SELECT detected_asn, expected_asn FROM possible_hijacks')
res1 = res1.fetchall()

c.close()
conn.close()

for i in res1:
    with open('victims.txt', 'a') as the_file:
        the_file.write(str(i[0])+"\n")
    with open('hijackers.txt', 'a') as the_file:
        the_file.write(str(i[1])+"\n") 
