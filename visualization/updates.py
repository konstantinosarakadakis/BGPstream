import json
import sqlite3

with open('blackholes.json') as f:
    data1 = json.load(f)

with open('blackholes_everything.json') as f:
    data3 = json.load(f)



conn = sqlite3.connect('record.db')
c = conn.cursor()

data_1_prefixes = []
data_2_prefixes = []
data_3_prefixes = []

for i in data1:
    res = c.execute('SELECT detected_prefix FROM possible_hijacks WHERE hash="{}"'.format(i['hash']))
    prefix = res.fetchall()[0][0]
    data_1_prefixes.append(prefix)


for i in data3:
    res = c.execute('SELECT detected_prefix FROM possible_hijacks WHERE hash="{}"'.format(i['hash']))
    prefix = res.fetchall()[0][0]
    data_3_prefixes.append( prefix)
c.close()
conn.close()

data2 = []

for i in range(0,len(data3)):
    if len(data3[i]['updates'])>0:
        data2.append(data3[i])
        data_2_prefixes.append(data_3_prefixes[i])


#print len(data_1_prefixes),len(data_2_prefixes),len(data_3_prefixes)

stable_1 = {} #blackholes
stable_2 = {} #bgpstream monitored
stable_3 = {} #bgpmon

for i in data_1_prefixes:
    try:
        stable_1[int(i.split("/")[1])] +=1
    except:
        stable_1[int(i.split("/")[1])] =1

for i in data_2_prefixes:
    try:
        stable_2[int(i.split("/")[1])] +=1
    except:
        stable_2[int(i.split("/")[1])] =1

for i in data_3_prefixes:
    try:
        stable_3[int(i.split("/")[1])] +=1
    except:
        stable_3[int(i.split("/")[1])] =1


for i in range(25,33):
    try:
        stable_1[i]+5
    except:
        stable_1[i] = 0

for i in range(25,33):
    try:
        stable_2[i]+5
    except:
        stable_2[i] = 0

for i in range(25,33):
    try:
        stable_3[i]+5
    except:
        stable_3[i] = 0



for i in range(25,33):
    sum_ = stable_3[i]
    if sum_ != 0:
        stable_1[i] = int((stable_1[i]*100)/float(sum_))
        stable_2[i] = int((stable_2[i]*100)/float(sum_))
        stable_3[i] = int((stable_3[i]*100)/float(sum_))



for i in range(25,33):
    print i,'\t:\t',stable_1[i],'\t',stable_2[i],'\t',stable_3[i]

for i in range(25,33):
    try:
        for k in range(0,stable_1[i]):
            with open('visual_blackholes.txt', 'a') as the_file:
                the_file.write(str(i-25)+"\n")
    except:
        pass

for i in range(25,33):
    try:
        for k in range(0,stable_2[i]-stable_1[i]):
            with open('visual_bgpstream.txt', 'a') as the_file:
                the_file.write(str(i-25)+"\n")
    except:
        pass


for i in range(25,33):
    try:
        for k in range(0,stable_3[i]-stable_2[i]):
            with open('visual_bgpmon.txt', 'a') as the_file:
                the_file.write(str(i-25)+"\n")
    except:
        pass
