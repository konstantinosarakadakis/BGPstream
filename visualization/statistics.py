import sqlite3
import datetime

conn = sqlite3.connect('record.db')
c = conn.cursor()

res1 = c.execute('SELECT * FROM possible_hijacks')
res1 = res1.fetchall()

res2 = c.execute('SELECT * FROM bgp_leaks')
res2 = res2.fetchall()

res3 = c.execute('SELECT * FROM outages')
res3 = res3.fetchall()

c.close()
conn.close()

print 

print len(res1), 'Possible hijacks {'

ipv4 = 0
ipv6 = 0

for i in res1:
	if ':' in i[13]:
		ipv6 +=1
	else: 
		ipv4 +=1

print '\n\tipv4 ',ipv4
print '\tipv6 ',ipv6
print 

against = {}

for i in res1:
	x = int(i[13].split('/')[1])
	try:
		against[x] += 1 
	except:
		against[x] = 1

for i in against.keys():
	print "\t/"+str(i)+"\t:\t"+str(against[i])

finished = 0
unfinished = 0

for i in res1:
	if i[8] == 'None':
		unfinished += 1
	else:
		finished += 1

print '\n\tFinished', finished
print '\tUnfinished', unfinished


avg_duration = 0

for i in res1:
	if i[8] != 'None':
		time_start = i[7].split(":")
		time_end = i[11].split(":")

		start = datetime.datetime(i[4],i[5],i[6],int(time_start[0]),int(time_start[1]),int(time_start[2]))
		end = datetime.datetime(i[8],i[9],i[10],int(time_end[0]),int(time_end[1]),int(time_end[2]))

		timestamp = int((end - start).total_seconds())
		
		avg_duration += timestamp

print "\tAverage duration", avg_duration/finished,"s"


print "}\n"


print len(res2), 'BGP Leaks {'

ipv4 = 0
ipv6 = 0

for i in res2:
	if ':' in i[13]:
		ipv6 +=1
	else: 
		ipv4 +=1

print '\n\tipv4 ',ipv4
print '\tipv6 ',ipv6


finished = 0
unfinished = 0


for i in res2:
	if i[8] == 'None':
		unfinished += 1
	else:
		finished += 1

print '\n\tFinished', finished
print '\tUnfinished', unfinished



for i in res2:
	if i[8] != 'None':
		time_start = i[7].split(":")
		time_end = i[11].split(":")

		start = datetime.datetime(i[4],i[5],i[6],int(time_start[0]),int(time_start[1]),int(time_start[2]))
		end = datetime.datetime(i[8],i[9],i[10],int(time_end[0]),int(time_end[1]),int(time_end[2]))

		timestamp = int((end - start).total_seconds())
		
		avg_duration += timestamp

print "\tAverage duration", (avg_duration/finished),"s"






print "}\n"


print len(res3), 'Outages {'

finished = 0
unfinished = 0

affected = {}


for i in res3:
	try: 
		affected[int(i[11])] += 1
	except:
		affected[int(i[11])] = 1

# for i in affected.keys():
# 	print "\t"+str(i)+"\t:\t"+str(affected[i])





for i in res3:
	if i[7] == 'None':
		unfinished += 1
	else:
		finished += 1








print '\n\tFinished', finished
print '\tUnfinished', unfinished

avg_duration = 0
for i in res3:
	if i[7] != 'None':
		time_start = i[6].split(":")
		time_end = i[10].split(":")

		start = datetime.datetime(i[3],i[4],i[5],int(time_start[0]),int(time_start[1]),int(time_start[2]))
		end = datetime.datetime(i[7],i[8],i[9],int(time_end[0]),int(time_end[1]),int(time_end[2]))

		timestamp = int((end - start).total_seconds())
		
		avg_duration += timestamp

print "\tAverage duration", avg_duration/finished,"s"

print "}"
