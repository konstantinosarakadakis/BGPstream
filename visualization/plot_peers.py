import matplotlib.pyplot as plt
import numpy as np
import csv

k=[]
zz = []


zz.append([0])


for index in range(2,62):
    temp_list = list()
    with open('monitored_peers.csv') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        for row in reader:
            temp_list.append(float(row[index])/float(row[1])   )

    zz.append(temp_list)


with open('monitored_peers.csv') as csv_file:
    reader = csv.reader(csv_file, delimiter=',')
    for row in reader:
        k.append(float(row[-1])/float(row[1]) )


for i in range(0,len(zz)):
  zz[i]=np.median(zz[i])


k = np.median(k)

for i in range(0,len(zz)):
    zz[i] = np.median(zz[i])


indexes = []
for i in range(0,122,2):
    indexes.append(i)

h = zz

ax = plt.axes(axisbg='#E6E6E6')
ax.set_axisbelow(True)

ax.xaxis.tick_bottom()
ax.yaxis.tick_left()

for tick in ax.get_xticklabels():
    tick.set_color('gray')
for tick in ax.get_yticklabels():
    tick.set_color('gray')


plt.grid(color='w', linestyle='solid')


plt.stackplot(indexes,h,colors=['#EE6666'],edgecolor='#E6E6E6')

plt.axhline(y=k, xmin=0, xmax=120, linewidth=2, color = 'r',linestyle='--') 
# plt.legend()
 
plt.rcParams.update({'font.size': 14})

plt.ylim(top=max(k,h[-1])+5)
ax.set_xlabel("Seconds after the hijack")
ax.set_ylabel("Median BGPstream - BGPmon monitored peers correlation")

plt.show()

