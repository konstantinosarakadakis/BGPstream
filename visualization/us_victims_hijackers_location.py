#!/usr/bin/env python
import sqlite3
import datetime
import csv
import os 

try:
    os.remove("us_victims_hijackers_minor.csv") 
except:
    pass
try:
    os.remove("us_victims_hijackers_major.csv") 
except:
    pass

with open('us_victims_hijackers_minor.csv', mode='a') as csv_file:
    fieldnames = ['longtitude', 'latitude', 'ASN']
    writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
    writer.writeheader()


with open('us_victims_hijackers_major.csv', mode='w') as csv_file:
    fieldnames = ['longtitude', 'latitude', 'ASN']
    writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
    writer.writeheader()

conn = sqlite3.connect('record.db')
c = conn.cursor()

res1 = c.execute('SELECT expected_asn, detected_asn FROM possible_hijacks')
res1 = res1.fetchall()

geo_ = {}

for hijack in res1:
	res2 = c.execute('SELECT country FROM geoloc WHERE ASN='+str(hijack[0]))
	res2 = res2.fetchall()
	if len(res2)!=0 and res2[0][0]=='US':
		try:
			geo_[hijack[1]] += 1
		except:
			geo_[hijack[1]] = 1


geo_major = []
geo_minor = []

for i in geo_:
	if geo_[i]<3:
		geo_minor.append(i)
	else:
		geo_major.append(i)


for i in geo_minor:
	res2 = c.execute('SELECT longtitude , latitude FROM geoloc WHERE ASN='+str(i))
	res2 = res2.fetchall()
	if len(res2)!=0:
		with open('us_victims_hijackers_minor.csv', mode='a') as csv_file:
		    writer = csv.writer(csv_file)
		    writer.writerow((res2[0][0],res2[0][1],str(i)))


for i in geo_major:
	res2 = c.execute('SELECT longtitude , latitude FROM geoloc WHERE ASN='+str(i))
	res2 = res2.fetchall()
	if len(res2)!=0:
		with open('us_victims_hijackers_major.csv', mode='a') as csv_file:
		    writer = csv.writer(csv_file)
		    writer.writerow((res2[0][0],res2[0][1],str(i)))




c.close()
conn.close()
