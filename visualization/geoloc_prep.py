#!/usr/bin/env python
import sqlite3
import datetime
import csv

conn = sqlite3.connect('record.db')
c = conn.cursor()

res1 = c.execute('SELECT detected_asn,expected_asn FROM possible_hijacks WHERE endtime_year!="None"')
res1 = res1.fetchall()

geo_detected = []
geo_expected = []

for hijack in res1:
	print hijack
	res2 = c.execute('SELECT longtitude , latitude FROM geoloc WHERE ASN='+str(hijack[0]))
	res2 = res2.fetchall()
	if len(res2)!=0:
		geo_detected.append(res2)

	res2 = c.execute('SELECT longtitude , latitude FROM geoloc WHERE ASN='+str(hijack[1]))
	res2 = res2.fetchall()
	if len(res2)!=0:
		geo_expected.append(res2)

c.close()
conn.close()

for k in geo_expected:
	with open('expected_location.csv', mode='a') as csv_file:
	    writer = csv.writer(csv_file)
	    writer.writerow(k)


for k in geo_detected:
	with open('detected_location.csv', mode='a') as csv_file:
	    writer = csv.writer(csv_file)
	    writer.writerow(k)
