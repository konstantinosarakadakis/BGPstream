#!/usr/bin/env python
"""Checks the validity of bgp leak as-paths and if these events should be treated as bgp leaks by bgpmon"""

import sqlite3

__author__ = "Arakadakis Konstantinos"
__license__ = "MIT"
__version__ = "1.0"
__maintainer__ = "Arakadakis Konstantinos"
__email__ = "arakadakiskonstantinos@gmail.com"
__status__ = "Development"


def get_code(data,i,y):
	try:
		return data[i,y]
	except:
		return 2


def bgp_leak_validation(db_path,record_path):
	"""Prints the ratio of the bgp leaks that were false positives 
	@ db_path = path where db is located
	@ record_path the actual path of the CAIDA AS-REL dataset e.g. .../.../.../.../.../20180801.as-rel2.txt"""
	
	conn = sqlite3.connect(db_path)
	c = conn.cursor()

	res = c.execute('SELECT leaked_aspath,detected_asn,leaked_to FROM bgp_leaks')
	#detected_asn = leaker ASN

	res = res.fetchall()

	c.close()
	conn.close()

	dataset = {}

	f = open(record_path, "r")  # Read CAIDA As-rel dataset

	as_rel = f.readlines()

	for i in as_rel:
		if i[0]=="#":
			continue
		temp = i.split("|")
		dataset[int(temp[0]),int(temp[1])] = int(temp[2])
		dataset[int(temp[1]),int(temp[0])] = -int(temp[2])  # Create forward and backward AS relationships

	# Codes 
	# -----------
	# 0  :  p2p
	# -1 :  p2c
	# 1  :  c2p
	# 2  :  N/A


	false_positives = []
	for leak in res: # for each BGP Leak ...
		
		as_path_temp = list(leak[0].split(' '))
		# get the as-path in the order the announcement was propagated
		# print as_path
		# continue
		policies = []

		for i in range(0, len(as_path_temp)):
			if int(as_path_temp[i]) == int(leak[1]):
				as_path = as_path_temp[i:-1]
				break

		policies = []

		policies.append(get_code(dataset,int(leak[2]) , int(as_path[0]))) # Add the relationship of leaked_to->leaker

		for i in range(0, len(as_path)-1): # for each ASi <-> ASi+1 add the type of relationship
			policies.append(get_code(dataset,int(as_path[i]) , int(as_path[i+1])))


		index = 0 
		# Acceptable as path format :   ( [c2p]*[p2p]?[p2c]* )

		while((index < len(policies)) and ((policies[index] == 1) or (policies[index]==2))): 
		#traverse all c2ps at the beginning of as-path
			index = index + 1
		
		if((index < len(policies)) and (policies[index]==0 or policies[index]==2 )):
		#traverse 1 or 0 p2p relationships
			index = index + 1

		while((index < len(policies)) and ((policies[index]==-1) or (policies[index]==2))):
		#traverse all p2cs at the end of as-path
			index = index + 1

			
		if index==len(policies):
			false_positives.append(policies)
			if not 2 in policies:
				print policies

	print "--------------------"
	print
	print "BGP Leaks\t\t:\t", len(res)
	print "False Positives\t:\t", len(false_positives)

if __name__ == '__main__':
	path1 = '/home/konstantinos/Desktop/BGP-stream_parser'
	path2 = '/home/konstantinos/Desktop/BGP-stream_parser/datasets/20180801.as-rel2.txt'
	bgp_leak_validation(path1, path2)