#!/usr/bin/env python
"""
Checks the effect of the hijacks to the cone of victim AS.
"""

import sqlite3
from _pybgpstream import BGPStream, BGPRecord, BGPElem
import datetime
import os 
import csv

__author__ = "Arakadakis Konstantinos"
__license__ = "MIT"
__version__ = "1.0"
__maintainer__ = "Arakadakis Konstantinos"
__email__ = "arakadakiskonstantinos@gmail.com"
__status__ = "Development"


def check_cone_effect(db_path, input_ppdc_path, out_csv_path='' ):
    """prints the effect of the hijack to the cone of the victim.
    @ db_path : the path of database
    @ out_csv_path : the dir of outputhe  file
    @ input_ppdc_path : the full path ppdc is located ( CAIDA ppdc file )"""
    already_checked = []
    if out_csv_path != '':
        out_csv_path += '/'
    if not os.path.exists(out_csv_path+"cone_affection.csv"):
        with open(out_csv_path+"cone_affection.csv", mode='a') as csv_file:
            fieldnames = ['Hijack ID', 'Cone size', 'Affected ASes']
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
            writer.writeheader()
    else:
        with open(out_csv_path+"cone_affection.csv") as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            k=0
            for row in csv_reader:
                if k == 0:
                    k += 1
                    continue
                already_checked.append(row[0])

     # Create a new bgpstream instance and a reusable bgprecord instance


    conn = sqlite3.connect(db_path)
    c = conn.cursor()

    res1 = c.execute('SELECT detected_prefix, starttime_year, starttime_month, starttime_day, starttime_time, detected_asn, expected_asn,hash FROM possible_hijacks')
    res1 = res1.fetchall()

    c.close()
    conn.close()

    f = open(input_ppdc_path, "r")
    asns = f.readlines()
    f.close()

    asns = asns[2:-1]
    cone = {}

    for k in asns:
        temp = k.split(' ')

        temp2 = []
        for z in range (1,len(temp)):
            temp2.append(int(temp[z]))
        cone[int(temp[0])] = temp2

    # EDW THA ARXISOUME NA DIALEGOUME HIJACKS
    ind = 0
    for hijack in res1:
        ind += 1
        if hijack[7] in already_checked:
            continue 
        tt = hijack[4].split(":")
        epoch = datetime.datetime(1970,1,1,0,0,0)
        now = datetime.datetime(hijack[1],hijack[2],hijack[3],int(tt[0]),int(tt[1]),int(tt[2]))
        timestamp = int((now - epoch).total_seconds())

     
        affected_ases = []

        stream = BGPStream()
        rec = BGPRecord()


        stream.add_interval_filter(timestamp,timestamp+5*60)

        # Start the stream
        stream.add_filter('record-type','updates')
        stream.add_filter('prefix',hijack[0])

        stream.start()

        while(stream.get_next_record(rec)):
            if rec.status == "valid":
                elem = rec.get_next_elem()
                while(elem):
                    # Print record and elem information
                    try:
                        if elem.type == 'A' and str(hijack[5]) in elem.fields['as-path'].split(' '):
                            temp_as_path = elem.fields['as-path'].split(' ')
                            for tt in temp_as_path:
                                int_temp = int(tt)
                                if not int_temp in affected_ases:
                                    affected_ases.append(int_temp)
                            # print rec.project, rec.collector, rec.type, rec.time, rec.status,
                            # print elem.type, elem.peer_address, elem.peer_asn, elem.fields
                    except:
                        pass
                    finally:
                        elem = rec.get_next_elem()

        zz = 0
        for as_ in affected_ases:
            try:
                if as_ in cone[hijack[6]]:
                    zz += 1
            except:
                zz = -50

        if zz<0:
            cone[hijack[6]] = []
            zz = 0

        try:
            print "Hijack:\t",hijack[7],"\tCone size:\t",len(cone[hijack[6]]),"\tCone ASes affected:\t",zz,'\t',ind,"/",len(res1)
            with open('cone_affection.csv', mode='a') as csv_file:
                writer = csv.writer(csv_file)
                writer.writerow((str(hijack[7]) , len(cone[hijack[6]]) , zz ))
        except:
            print "Hijack:\t",hijack[7],"\tCone size:\t",0,"\tCone ASes affected:\t",0,'\t',ind,"/",len(res1)
            with open('cone_affection.csv', mode='a') as csv_file:
                writer = csv.writer(csv_file)
                writer.writerow((str(hijack[7]) , 0 , 0 ))


if __name__ == '__main__':
    path_db = '/home/konstantinos/Desktop/BGP-stream_parser'
    path_in = '/home/konstantinos/Desktop/BGP-stream_parser/datasets/20180801.ppdc-ases.txt'
    check_cone_effect(path_db,path_in)
    print "kostas"