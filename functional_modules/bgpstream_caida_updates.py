#!/usr/bin/env python
"""Save monitored by bgpstream updates to json file"""
import sqlite3
from _pybgpstream import BGPStream, BGPRecord, BGPElem
import datetime
import json
import json


def all_updates(db_path, out_json_path):
    """Save monitored by bgpstream updates to json file"""
    table = []
    conn = sqlite3.connect(db_path)
    c = conn.cursor()

    res1 = c.execute('SELECT detected_prefix, starttime_year, starttime_month, starttime_day, starttime_time,hash FROM possible_hijacks')
    res1 = res1.fetchall()

    c.close()
    conn.close()

    new_ = []
    for i in res1:
        if int(i[0].split("/")[1])>24 and ':' not in i[0]:
            new_.append(i)

    # EDW THA ARXISOUME NA DIALEGOUME HIJACKS
    final_json=[]
    z = 0

    for hijack in new_:
        z += 1
        print ">   ",z,"/",len(new_)
        tt = hijack[4].split(":")


        epoch = datetime.datetime(1970,1,1,0,0,0)
        now = datetime.datetime(hijack[1],hijack[2],hijack[3],int(tt[0]),int(tt[1]),int(tt[2]))
        timestamp = int((now - epoch).total_seconds())

        stream = BGPStream()
        rec = BGPRecord()

        stream.add_interval_filter(timestamp-60,timestamp+180) # -1 min <-> +3 mins

        # Start the stream
        stream.add_filter('record-type','updates')
        stream.add_filter('prefix',hijack[0])

        stream.start()

        json_to_store = {"hash":hijack[5], "updates":[]}

        while(stream.get_next_record(rec)):
            # Print the record information only if it is not a valid record
            if rec.status != "valid":
                pass
            else:
                elem = rec.get_next_elem()
                while(elem):
                    # Print record and elem information
                    try:
                        if elem.type == 'A':    
                            json_to_store['updates'].append(elem.fields)
                    except:
                        pass
                    finally:
                        elem = rec.get_next_elem()

        final_json.append(json_to_store)
        
        with open(out_json_path+'/bgpstream_updates24.json', 'w') as outf:
            json.dump(final_json, outf, indent=4)