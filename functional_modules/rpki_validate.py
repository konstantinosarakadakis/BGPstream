#!/usr/bin/python
"""
Check the ratio of the prefixes that were hijacked AND were also protected by RPKI
This script uses RIPE NCC localcert as RPKI ROAs repository.
"""

import json
import urllib2
import sqlite3
import os

__author__ = "Arakadakis Konstantinos"
__license__ = "MIT"
__version__ = "1.0"
__maintainer__ = "Arakadakis Konstantinos"
__email__ = "arakadakiskonstantinos@gmail.com"
__status__ = "Development"


def check_rpki(db_path):
	"""Depict the number of possible hijacks whos expected_prefix is protected by RPKI"""
	try:
		conn = sqlite3.connect(db_path)
		c = conn.cursor()
		res_possible_hijack = c.execute('SELECT expected_asn,expected_prefix ,detected_prefix FROM possible_hijacks')
		res_possible_hijack = res_possible_hijack.fetchall()
		c.close()
		conn.close()

		coveved_by_ROA = 0

		for i in res_possible_hijack:
			try:
				print "."
				str_ = "http://localcert.ripe.net:8088/api/v1/validity/AS"+str(i[0])+"/"+str(i[2])
				v1 = json.load(urllib2.urlopen(str_))
				if v1['validated_route']['validity']['state'] == 'Valid' :
					coveved_by_ROA+=1
					print i[2]
					print v1['validated_route']['validity']['VRPs']['matched'][0]
			except:
				pass	
			 
		print 'All hijacks\t:\t',len(res_possible_hijack)
		print 'Protected prefixes\t:\t',coveved_by_ROA
	except:
		print "Cannot run check_rpki"


if __name__ == '__main__':
	path = os.path.abspath(os.path.dirname(__file__))
	check_rpki(path)