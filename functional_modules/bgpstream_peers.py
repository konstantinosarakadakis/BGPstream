#!/usr/bin/env python
"""Depicts the peers that announced the event to the bgpstream(caida) monitors in contrary to the peers that bgpmon
claims to have received the announcement from"""
import sqlite3
from _pybgpstream import BGPStream, BGPRecord, BGPElem
import datetime
import csv

__author__ = "Arakadakis Konstantinos"
__license__ = "MIT"
__version__ = "1.0"
__maintainer__ = "Arakadakis Konstantinos"
__email__ = "arakadakiskonstantinos@gmail.com"
__status__ = "Development"

def monitored_peers(db_path, output_csv_path):
    """Cretes monitored peers for ploting"""
    conn = sqlite3.connect(db_path)
    c = conn.cursor()

    res1 = c.execute('SELECT detected_prefix, starttime_year, starttime_month, starttime_day, starttime_time, detected_asn, expected_asn,endtime_year, endtime_month, endtime_day, endtime_time,peers,expected_prefix,hash FROM possible_hijacks WHERE endtime_year!="None"')
    res1 = res1.fetchall()

    c.close()
    conn.close()

    print "BGP Hijacks duration"
    bottom = int(input("From (seconds) : ")) 
    ceil = int(input("To (seconds) : "))  # duration of event

    res2 = []

    for hijack in res1:

        tt = hijack[4].split(":")
        t1 = hijack[10].split(":")

        epoch = datetime.datetime(1970,1,1,0,0,0)
        start = datetime.datetime(hijack[1],hijack[2],hijack[3],int(tt[0]),int(tt[1]),int(tt[2]))
        timestamp = int((start - epoch).total_seconds())


        end = datetime.datetime(hijack[7],hijack[8],hijack[9],int(t1[0]),int(t1[1]),int(t1[2]))

        if int((end - start).total_seconds())<=ceil and int((end - start).total_seconds())>bottom:
            res2.append(hijack)

    ind = 1
    print len(res2)

    ceil = 120

    for hijack in res2:
        print hijack,ind, "/",len(res2)
        ind += 1
        tt = hijack[4].split(":")
        t1 = hijack[10].split(":")
        epoch = datetime.datetime(1970,1,1,0,0,0)
        start = datetime.datetime(hijack[1],hijack[2],hijack[3],int(tt[0]),int(tt[1]),int(tt[2]))
        timestamp = int((start - epoch).total_seconds())

        total_monitors = 0
        records = []
        records_temp = []

        for k in range(0,ceil,5):
            print ">> Interval ["+str(k)+","+str(k+5)+"] "+ str((100*k)/float(ceil))+" %"
            stream = BGPStream()
            rec = BGPRecord()
            stream.add_interval_filter(timestamp+k,timestamp+5+k)

            # Start the stream
            stream.add_filter('record-type','updates')
            stream.add_filter('prefix',hijack[0])
            stream.start()

            while(stream.get_next_record(rec)):
                # Print the record information only if it is not a valid record
                if rec.status != "valid":
                    pass
                else:
                    elem = rec.get_next_elem()
                    while(elem):
                        # Print record and elem information
                        try:
                            if elem.type == 'A' and rec.collector not in records_temp:
                                records_temp.append(rec.collector)
                        except:
                            pass
                        finally:
                            elem = rec.get_next_elem()
            records.append(len(records_temp))
        print ">>>",records
        records.insert(0,hijack[11])
        records.insert(0,hijack[13])

        with open(output_csv_path+'/monitored_peers.csv', mode='a') as csv_file:
            writer = csv.writer(csv_file)
            writer.writerow(records)