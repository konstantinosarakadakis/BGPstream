#!/usr/bin/python
"""
Main script to conduct analysis
"""

from functional_modules import *
import os

__author__ = "Arakadakis Konstantinos"
__license__ = "MIT"
__version__ = "1.0"
__maintainer__ = "Arakadakis Konstantinos"
__email__ = "arakadakiskonstantinos@gmail.com"
__status__ = "Development"


def return_file_coloured(path):
    if(not os.path.exists(path)):
        return "\033[1;31m{}\033[0m".format(path.split('/')[-1])
    else:
        return "\033[1;32m{}\033[0m".format(path.split('/')[-1])

def find_ppdc(path):
    files = os.listdir(path)
    for file in files:
        if 'ppdc' in file:
            return path+'/'+file
    return ''



def find_as_rel(path):
    files = os.listdir(path)
    for file in files:
        if 'as-rel2' in file:
            return path+'/'+file
    return ''

def main(path):
    """Main function"""
    try:
        print "1. RPKI Analysis\t\t[",
        dependencies = [path+'/record.db']
        for i in dependencies:
            print return_file_coloured(i)+',',
        print ']'
        print "2. Geographic correlation\t[",

        dependencies = [path+'/record.db']
        for i in dependencies:
            print return_file_coloured(i)+',',
        print ']'

        print "3. Cone Analysis\t\t[",
        dependencies = [path+'/record.db',find_ppdc(path+'/datasets')]
        for i in dependencies:
            print return_file_coloured(i)+',',
        print ']'

        print "4. Blackholes Analysis\t\t[",
        dependencies = [path+'/record.db',path+'/datasets/blackholes.json']
        for i in dependencies:
            print return_file_coloured(i)+',',
        print ']'

        print "5. Blackhole datasets\t\t[",
        dependencies = [path+'/datasets/bgpstream_updates24.json']
        for i in dependencies:
            print return_file_coloured(i)+',',
        print ']'

        print "6. Impact analysis (Peers)\t[",
        dependencies = [path+'/record.db']
        for i in dependencies:
            print return_file_coloured(i)+',',
        print ']'

        print "7. BGPstream CAIDA updates\t[",
        dependencies = [path+'/record.db']
        for i in dependencies:
            print return_file_coloured(i)+',',
        print ']'

        print "8. BGP Leaks violation \t\t[",
        dependencies = [path+'/record.db', find_as_rel(path+'/datasets')]
        for i in dependencies:
            print return_file_coloured(i)+',',
        print ']'

        print 
        opt = input("Option : ")
        
        if opt == 1:
            check_rpki(path+'/record.db')
        elif opt == 2:
            geo_correlate(path+'/record.db')
        elif opt == 3:
            check_cone_effect(path+'/record.db', find_ppdc(path+'/datasets'), path+'/datasets')
        elif opt == 4:
            blackholes_analysis(path+'/datasets/blackholes.json',path+'/record.db',path+'/datasets')
        elif opt == 5:
            create_datasets(path+'/datasets')
        elif opt == 6:
            monitored_peers(path+'/record.db', path+'/datasets')
        elif opt == 7:
            all_updates(path+'/record.db', path+'/datasets')
        elif opt == 8:
            bgp_leak_validation(path+'/record.db', find_as_rel(path+'/datasets'))
        else:
            print "\033[1;31mNot valid input!\033[0m"
    except:
        print "\033[1;31m[Error occured]\033[0m" 
        
if __name__ == '__main__':
    path = os.path.abspath(os.path.dirname(__file__))
    main(path)
     