#!/usr/bin/python
"""
Parses the events that are uploaded on BGPstram( BGPmon service )
and stores them into a database [record.db] and json [record.json]

**Warning** Json file contains duplicates.
"""

import requests 
import json
import sqlite3
from hashlib import sha256 as SHA256
import subprocess
import urllib2
import os

__author__ = "Arakadakis Konstantinos"
__license__ = "MIT"
__version__ = "1.0"
__maintainer__ = "Arakadakis Konstantinos"
__email__ = "arakadakiskonstantinos@gmail.com"
__status__ = "Development"


def create_db(local_path):
    """Initialize the database"""
    try:
        conn = sqlite3.connect(local_path+'/record.db')
        c = conn.cursor()
        c.execute("CREATE TABLE IF NOT EXISTS outages (hash VARCHAR PRIMARY KEY\
        , country VARCHAR, outage_asn INTEGER, starttime_year INTEGER, \
        starttime_month INTEGER,  starttime_day INTEGER, \
        starttime_time VARCHAR, endtime_year INTEGER, endtime_month INTEGER,  \
        endtime_day INTEGER, endtime_time VARCHAR, affected_prefixes INTEGER)")
        
        c.execute("CREATE TABLE IF NOT EXISTS bgp_leaks (\
        hash VARCHAR PRIMARY KEY, country VARCHAR, expected_asn INTEGER,\
        detected_asn INTEGER, starttime_year INTEGER, starttime_month INTEGER,\
        starttime_day INTEGER, starttime_time VARCHAR, endtime_year INTEGER,\
        endtime_month INTEGER, endtime_day INTEGER, endtime_time VARCHAR,\
        peers INTEGER, leaked_prefix VARCHAR, leaked_aspath VARCHAR,\
        leaked_to INTEGER)")


        c.execute("CREATE TABLE IF NOT EXISTS possible_hijacks (\
        hash VARCHAR PRIMARY KEY, country VARCHAR, expected_asn INTEGER, \
        detected_asn INTEGER, starttime_year INTEGER, starttime_month INTEGER, \
        starttime_day INTEGER, starttime_time VARCHAR, endtime_year INTEGER,\
        endtime_month INTEGER,  endtime_day INTEGER, endtime_time VARCHAR,\
        peers INTEGER, expected_prefix VARCHAR, detected_prefix VARCHAR,\
        detected_aspath VARCHAR)")

        c.execute("CREATE TABLE IF NOT EXISTS geoloc (ASN INTEGER PRIMARY KEY,\
        country VARCHAR, longtitude INTEGER, latitude INTEGER)")

        conn.commit()

    except:
        print("\033[1;31m[Error] Could not initialize Database.\033[0m")
        c.close()
        conn.close()
        exit()
    finally:
        c.close()
        conn.close()

def get_website():
    """Download and parse the website content."""
    try:
        req = requests.get("https://bgpstream.com/").content.split("<tbody>")[1]
        req = req.replace('</tr>','</tr>|||')
        req = req.replace("<tr>","{")
        req = req.replace("</tr>",'"dummy":"dummy"}')
        req = req.replace('<td class="event_type">','"event_type":"')
        req = req.replace('<td class="country">','"country":"')
        req = req.replace('<td class="asn">','"asn":"')
        req = req.replace('<td class="starttime">','"starttime":"')
        req = req.replace('<td class="endtime">','"endtime":"')
        req = req.replace('<td class="moredetail">','"moredetail":')
        req = req.replace('</td>','",')
        req = req.replace('<img src="','')
        req = req.replace('gif">','')
        req = req.replace('&amp','')
        req = req.replace('&#39','')
        req = req.replace('<a href=','')
        req = req.replace('>More detail</a>','')
        req = req.replace("</i>","")
        req = req.replace("<i>","")
        req = req.replace('<br>',"")
        req = req.replace('""','"')
        req = req.replace('\n','')
        req = req.replace('\t','')
        req = req.split('|||')
        return req
    except:
        return []


def update_db(last_hash, local_path):
    """Store the content in the database and json file."""
    has_encounterd_last_entry = False
    try:
        with open(local_path+'/record.json') as f:
            all_jsons = json.load(f)
    except:
        all_jsons = []
    req = get_website()

    for i in range(len(req)-2, -1, -1):  # Pass the last event because it is not valid 
        try:
            temp_json = json.loads(req[i])
            temp_json['moredetail'] = "https://bgpstream.com" + temp_json['moredetail'] ## Load the more details of each event

            if(len(temp_json['country'].replace(' ','')) == 0):
                temp_json['country'] = None
            else:
                temp_json['country'] = temp_json['country'].split('  ')[0][1:]

            if temp_json['event_type'] != 'Possible Hijack':
                temp_json['event_type'] = temp_json['event_type'][:-1]

            if temp_json['asn'].replace(' ','') == 'N/A':
                temp_json['detected asn'] = None
                temp_json['expected asn'] = None
                temp_json['outage asn'] = None
            else:
                if temp_json['event_type'] != 'Outage':
                    try:
                        temp_json['detected asn'] = temp_json['asn'].split("(AS ")[2].split(')')[0]
                        temp_json['expected asn'] = temp_json['asn'].split("(AS ")[1].split(')')[0]
                    except:
                        pass
                else:
                    try:
                        temp_json['outage asn'] = temp_json['asn'].split("(AS ")[1].split(')')[0]
                    except:
                        pass

            strattime_temp = temp_json['starttime'].split(' ')
            temp_json['starttime_year'] = strattime_temp[1].split('-')[0]
            temp_json['starttime_month'] = strattime_temp[1].split('-')[1]
            temp_json['starttime_day'] = strattime_temp[1].split('-')[2]
            temp_json['starttime_time'] = strattime_temp[2]

            temp_json['endtime'] = temp_json['endtime'][2:]

            if(len(temp_json['endtime'].replace(' ','')) == 0):  
                temp_json['endtime'] = None
                temp_json['endtime_year'] = None
                temp_json['endtime_month'] = None
                temp_json['endtime_day'] = None
                temp_json['endtime_time'] = None
            else: 
                endtime_temp = temp_json['endtime'].split(' ')
                temp_json['endtime_year'] = endtime_temp[1].split('-')[0]
                temp_json['endtime_month'] = endtime_temp[1].split('-')[1]
                temp_json['endtime_day'] = endtime_temp[1].split('-')[2]
                temp_json['endtime_time'] = endtime_temp[2]

            text_for_hash = temp_json['event_type']
            try:
                if temp_json['event_type'] != 'Outage':
                    text_for_hash = text_for_hash + temp_json['detected asn'] + temp_json['expected asn']
                else:
                    text_for_hash = text_for_hash + temp_json['outage asn']
            except:
                pass

            text_for_hash = text_for_hash + temp_json['starttime']
            hash_ = SHA256(text_for_hash).hexdigest()

            if temp_json['event_type'] == 'Possible Hijack':
                print i+1, '/',len(req)-1, temp_json['event_type'],'\t',
            else:
                print i+1, '/',len(req)-1, temp_json['event_type'],'\t\t',


            if last_hash!=None:
                if not has_encounterd_last_entry:
                    if hash_ == last_hash:
                        has_encounterd_last_entry = True

                if not has_encounterd_last_entry:
                    print "\033[1;30m[Passed]\033[0m"
                    continue

            conn = sqlite3.connect(local_path+'/record.db')
            c = conn.cursor()

            if temp_json['event_type'] == 'Possible Hijack':
                res = c.execute('SELECT * FROM possible_hijacks WHERE hash="{}"'.format(hash_))
                res = res.fetchall()
            elif temp_json['event_type'] == 'BGP Leak':
                res = c.execute('SELECT * FROM bgp_leaks WHERE hash="{}"'.format(hash_))
                res = res.fetchall()
            elif temp_json['event_type'] == 'Outage':
                res = c.execute('SELECT * FROM outages WHERE hash="{}"'.format(hash_))
                res = res.fetchall()

            c.close()
            conn.close()
            if len(res)!=0:
                print "\033[1;33m[Updated]\033[0m"
                conn = sqlite3.connect(local_path+'/record.db')
                c = conn.cursor()

                if temp_json['event_type'] == 'Possible Hijack':
                    c.execute('DELETE FROM possible_hijacks WHERE hash = "{}"'.format(hash_))
                    conn.commit()

                elif temp_json['event_type'] == 'BGP Leak':
                    c.execute('DELETE FROM bgp_leaks WHERE hash = "{}"'.format(hash_))
                    conn.commit()

                elif temp_json['event_type'] == 'Outage':
                    c.execute('DELETE FROM outages WHERE hash = "{}"'.format(hash_))
                    conn.commit()

                c.close()
                conn.close()

            else:
                if temp_json['event_type'] == 'Possible Hijack':
                    string_ = 'BGP hijack  '+str(temp_json['detected asn'])+' -> '+\
                    str(temp_json['expected asn'])
                    subprocess.Popen(['notify-send', string_])
                print "\033[1;32m[Added]\033[0m"

            conn = sqlite3.connect(local_path+'/record.db')
            c = conn.cursor()

            # For geoloc purposes
            if temp_json['event_type'] == 'Possible Hijack': # Add tehe geolocation info of hijacker and victim
                try:
                    str_ = "http://as-rank.caida.org/api/v1/asns/"+temp_json['expected asn']
                    v1 = json.load(urllib2.urlopen(str_))
                    c.execute('INSERT INTO geoloc VALUES({},"{}",{},{})'.\
                    format(int(temp_json['expected asn']), v1['data']['country'],\
                    v1['data']['longitude'],v1['data']['latitude']))
                    conn.commit()
                except:
                    pass
                    
                try:
                    str_ = "http://as-rank.caida.org/api/v1/asns/"+temp_json['detected asn']
                    v1 = json.load(urllib2.urlopen(str_))
                    c.execute('INSERT INTO geoloc VALUES({},"{}",{},{})'.\
                    format(int(temp_json['detected asn']), v1['data']['country'],\
                    v1['data']['longitude'],v1['data']['latitude']))
                    conn.commit()
                except:
                    pass

            elif temp_json['event_type'] == 'BGP Leak':
                try:
                    str_ = "http://as-rank.caida.org/api/v1/asns/"+temp_json['expected asn']
                    v1 = json.load(urllib2.urlopen(str_))
                    c.execute('INSERT INTO geoloc VALUES({},"{}",{},{})'.\
                    format(int(temp_json['expected asn']), v1['data']['country']\
                    ,v1['data']['longitude'],v1['data']['latitude']))
                    conn.commit()
                except:
                    pass
                    
                try:
                    str_ = "http://as-rank.caida.org/api/v1/asns/"+temp_json['detected asn']
                    v1 = json.load(urllib2.urlopen(str_))
                    c.execute('INSERT INTO geoloc VALUES({},"{}",{},{})'.\
                    format(int(temp_json['detected asn']), v1['data']['country']\
                    ,v1['data']['longitude'],v1['data']['latitude']))
                    conn.commit()
                except:
                    pass

            elif temp_json['event_type'] == 'Outage':
                try:
                    str_ = "http://as-rank.caida.org/api/v1/asns/"+temp_json['outage asn']
                    v1 = json.load(urllib2.urlopen(str_))
                    c.execute('INSERT INTO geoloc VALUES({},"{}",{},{})'.\
                    format(int(temp_json['outage asn']), v1['data']['country'],\
                    v1['data']['longitude'],v1['data']['latitude']))
                    conn.commit()
                except:
                    pass

            # Event More detail manipulation
            temp_more = requests.get(temp_json['moredetail']).content
            conn = sqlite3.connect(local_path+'/record.db')
            c = conn.cursor()

            if temp_json['event_type'] == 'Possible Hijack':
                temp_json['expected_prefix'] = temp_more.split('Expected prefix: ')[1].split('</td>')[0]
                temp_json['detected_prefix'] = temp_more.split('Detected advertisement: ')[1].split('</td>')[0]
                temp_json['peers'] = int(temp_more.split('Detected by number of BGPMon peers: ')[1].split('</td>')[0])
                temp_json['detected_aspath'] = temp_more.split('AS Path ')[1].split('</td>')[0]

                try:
                    c.execute('INSERT INTO possible_hijacks VALUES("{}","{}",\
                    "{}","{}", "{}","{}","{}","{}","{}","{}","{}","{}","{}","{}"\
                    ,"{}","{}")'.format(hash_, temp_json['country'],
                    temp_json['expected asn'], temp_json['detected asn'], \
                    temp_json['starttime_year'], temp_json['starttime_month'],\
                    temp_json['starttime_day'], temp_json['starttime_time'],\
                    temp_json['endtime_year'], temp_json['endtime_month'],\
                    temp_json['endtime_day'], temp_json['endtime_time'],\
                    temp_json['peers'], temp_json['expected_prefix'],\
                    temp_json['detected_prefix'] , temp_json['detected_aspath']))
                    conn.commit()
                except:
                    print "Error inserting Possible hijack"

            elif temp_json['event_type'] == 'BGP Leak':
                temp_json['leaked_prefix'] = temp_more.split('Leaked prefix: ')[1].split(' (')[0]
                temp_json['peers'] = int(temp_more.split('Number of BGPMon peers that saw it: ')[1].split('</td>')[0])
                temp_json['detected_aspath'] = temp_more.split('AS path: ')[1].split('</td>')[0]
                temp_json['leaked_to'] = temp_more.split('Leaked To:<br> \n\t\t\t\t<li>')[1].split(' (')[0]

                try:
                    c.execute('INSERT INTO bgp_leaks VALUES("{}","{}","{}","{}",\
                    "{}","{}","{}","{}","{}","{}","{}","{}","{}","{}","{}","{}"\
                    )'.format(hash_, temp_json['country'], temp_json['expected asn'],\
                    temp_json['detected asn'] ,temp_json['starttime_year'],\
                    temp_json['starttime_month'],temp_json['starttime_day'],\
                    temp_json['starttime_time'], temp_json['endtime_year'], \
                    temp_json['endtime_month'], temp_json['endtime_day'], \
                    temp_json['endtime_time'], temp_json['peers'],\
                    temp_json['leaked_prefix'], temp_json['detected_aspath'],\
                    temp_json['leaked_to'] ))
                    conn.commit()
                except:
                    print "Error inserting BGP Leak"    

            elif temp_json['event_type'] == 'Outage':
                temp_json['affected_prefixes'] = int(temp_more.split('Number of Prefixes Affected: ')[1].split(' (')[0])
                
                try:
                    c.execute('INSERT INTO outages VALUES("{}","{}","{}","{}",\
                    "{}","{}","{}","{}","{}","{}","{}","{}")'.\
                    format(hash_, temp_json['country'], temp_json['outage asn']\
                    ,temp_json['starttime_year'], temp_json['starttime_month'],\
                    temp_json['starttime_day'], temp_json['starttime_time'],\
                    temp_json['endtime_year'], temp_json['endtime_month'],\
                    temp_json['endtime_day'], temp_json['endtime_time'],\
                    temp_json['affected_prefixes']))
                    conn.commit()
                except:
                    print "Error inserting Outage"  
                
            c.close()
            conn.close()

            ## Create new json and add it 
            del temp_json['dummy']
            del temp_json['endtime']
            del temp_json['starttime']
            del temp_json['asn']

            try:
                temp_json['endtime_year'] = int(temp_json['endtime_year'])
            except:
                pass
            try:
                temp_json['endtime_month'] = int(temp_json['endtime_month'])
            except:
                pass
            try:
                temp_json['endtime_day'] = int(temp_json['endtime_day'])
            except:
                pass
            try:
                temp_json['starttime_year'] = int(temp_json['starttime_year'])
            except:
                pass
            try:
                temp_json['starttime_month'] = int(temp_json['starttime_month'])
            except:
                pass
            try:
                temp_json['starttime_day'] = int(temp_json['starttime_day'])
            except:
                pass
            try:
                temp_json['expected asn'] = int(temp_json['expected asn'])
            except:
                pass
            try:
                temp_json['detected asn'] = int(temp_json['detected asn'])
            except:
                pass
            try:
                temp_json['outage asn'] = int(temp_json['outage asn'])
            except:
                pass
            try:
                temp_json['detected asn'] = int(temp_json['detected asn'])
            except:
                pass
            try:
                temp_json['leaked_to'] = int(temp_json['leaked_to'])
            except:
                pass
            try:
                temp_json['detected_aspath'] = temp_json['detected_aspath'].split(' ')
                for index in range(0,len(temp_json['detected_aspath'])):
                    temp_json['detected_aspath'][index] = int( temp_json['detected_aspath'][index]  )
            except:
                pass
            try:
                temp_json['detected_aspath'].remove('')
            except:
                pass
        except KeyboardInterrupt:
            print "\n\033[1;32m--END--\033[0m"
            with open(local_path+'/record.json', 'w') as outf:
                json.dump(all_jsons, outf, indent=4)
            exit()
        except:
            print "\033[1;31m[ERROR]\033[0m"
        else:
            all_jsons.append(temp_json)

    with open(local_path+'/record.json', 'w') as outf:
        json.dump(all_jsons, outf, indent=4)


def get_last_entered(local_path):
    """ Return the hash of the last entered entry Returns the last outage event
    Outage events have been chosen to represent the events because they are the 
    most common and there is at least one outage event each day.
    """
    conn = sqlite3.connect(local_path+'/record.db')
    c = conn.cursor()

    res = c.execute("""SELECT hash,starttime_time FROM 
    (SELECT * FROM (SELECT * FROM outages WHERE starttime_year = 
    (SELECT MAX(starttime_year) FROM outages)) WHERE starttime_month=
    (SELECT MAX(starttime_month) FROM 
    (SELECT * FROM outages WHERE starttime_year = 
    (SELECT MAX(starttime_year) FROM outages)))) WHERE starttime_day = 
    (SELECT MAX(starttime_day) FROM(SELECT * FROM 
    (SELECT * FROM outages WHERE starttime_year = 
    (SELECT MAX(starttime_year) FROM outages)) WHERE starttime_month=
    (SELECT MAX(starttime_month) FROM 
    (SELECT * FROM outages WHERE starttime_year =
    (SELECT MAX(starttime_year) FROM outages)))))""")

    res_outages = res.fetchall()

    c.close()
    conn.close()

    res_final = res_outages

    if  not res_final:
        return None
    else:
        hours = []
        for i in res_final:
            time = map(int,i[1].split(':'))
            hours.append(time[0])
        return res_final[hours.index(max(hours))][0]


if __name__ == '__main__':
    path = os.path.abspath(os.path.dirname(__file__))
    create_db(path)
    last_entry = get_last_entered(path)
    update_db(last_entry, path)